Enter the  "src" directory.  Type "make installwin" within Cygwin to
make Rifiuti for Windows.  Type "make install" to make Rifiuti for Unix.
The binaries will be located in the "bin" directory.

Keith J. Jones
keith.jones@foundstone.com

